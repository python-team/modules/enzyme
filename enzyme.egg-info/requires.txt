
[:python_version == "3.8"]
importlib_resources>=4.6

[dev]
doc8
mypy
ruff
typos
validate-pyproject
tox

[docs]
sphinx
sphinx-rtd-theme
myst-parser

[test]
PyYAML
requests
mypy
pytest>=6.0

[test:python_version < "3.10"]
importlib_metadata>=4.6
